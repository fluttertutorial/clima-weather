

import 'location.dart';
import 'networking.dart';

class WeatherModel {

  Future<dynamic> getLocationWeather() async {
    Location location = Location();
    await location.getCurrentLocation();

    NetworkHelper networkHelper = NetworkHelper(url: "https://api.openweathermap.org/data/2.5/weather?lat=${location.latitude}&lon=${location.longitude}&appid=d0cfb89ee4041eae178d0aeabac80c59&units=metric");

    var networkData = await networkHelper.getData();
    return networkData;
  }

  Future<dynamic> getCityWeather (String cityName) async {
    var url = "https://api.openweathermap.org/data/2.5/weather?q=$cityName&appid=d0cfb89ee4041eae178d0aeabac80c59&units=metric";

    NetworkHelper networkHelper = NetworkHelper(url: url);

    var networkData = await networkHelper.getData();
    return networkData;
  }


  String getWeatherIcon(int condition) {
    if (condition < 300) {
      return '🌩';
    } else if (condition < 400) {
      return '🌧';
    } else if (condition < 600) {
      return '☔️';
    } else if (condition < 700) {
      return '☃️';
    } else if (condition < 800) {
      return '🌫';
    } else if (condition == 800) {
      return '☀️';
    } else if (condition <= 804) {
      return '☁️';
    } else {
      return '🤷‍';
    }
  }

  String getMessage(int temp) {
    if (temp > 25) {
      return 'It\'s 🍦 time';
    } else if (temp > 20) {
      return 'Time for shorts and 👕';
    } else if (temp < 10) {
      return 'You\'ll need 🧣 and 🧤';
    } else {
      return 'Bring a 🧥 just in case';
    }
  }
}
